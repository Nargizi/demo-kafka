package com.crocusoft.kafkademo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class Consumer {
    @KafkaListener(topics = "demo",groupId = "crocusoft")
    public void listenGroupFoo(String message){
        log.info("Received message {}",message);

    }
}
