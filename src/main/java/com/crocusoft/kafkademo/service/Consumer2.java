//package com.crocusoft.kafkademo.service;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.stereotype.Service;
//
//@Slf4j
//@Service
//public class Consumer2 {
//    @KafkaListener(topics = "demo",groupId = "crocusoft2")
//    public void listenGroupFoo(String message){
//        log.info("Received message2 {}",message);
//
//    }
//}
