package com.crocusoft.kafkademo.service;

import com.crocusoft.kafkademo.dto.MailMessageDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Slf4j
@RequiredArgsConstructor
@Service
public class Producer {

    private final KafkaTemplate<String, MailMessageDto> kafkaTemplate;

    private long messageId;

    @Scheduled(fixedRate = 1000)
    public void produceMessages() {
        log.info("send message");
//        messageId++;
//        kafkaTemplate.send("demo",messageId+ " Hello-Kafka"+ LocalDateTime.now());
        kafkaTemplate.send("demo", "mail-message", new MailMessageDto("n@gmail.com", "Learn kafka", "introduction kafka"));
    }

}
